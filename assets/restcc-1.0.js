/*
var script = document.createElement('script');
script.src = "https://songbirdstag.cardinalcommerce.com/cardinalcruise/v1/songbird.js";
script.async = false; // This is required for synchronous execution
document.head.appendChild(script);
*/


var SecureCard = /** @class */ (function () {
    function SecureCard(cc) {

    
        if (!cc) {
            throw new Error('could not find the initial data');
        }
        this.cc = JSON.parse(cc);
        this.orderObject ={
                          Consumer: {
                            Account: {
                              AccountNumber: this.cc.Account.AccountNumber
                            }
                          }
                        };

        //console.log("card" + this.cc);
        
    }

	SecureCard.prototype.enable= function(){
			//console.log(JSON.stringify(this.cc));
    
            var _this = this;
            
            var jwt = "";
			var enrollobj = "";
			var transactionId = "";
            var cc = this.cc;
            var referenceId = Date.now();
            var refId = {"referenceId":referenceId};
    

            var ccdata = {...cc,
                          ...refId
                         };
            //console.log(ccdata);

            (function() {


                fetch("ClaimRequest.php", {
                    method: "POST", // *GET, POST, PUT, DELETE, etc.
  
                    body: JSON.stringify(ccdata), // body data type must match "Content-Type" header
			 })
            .then(r =>  r.json())
			.then(jwt =>{
                       
                        this.SecureCard.prototype.initCCA(jwt);
                     })
			.catch(error => console.log(JSON.stringify(error)));
            })();

			fetch("CardAuthEnrollService.php", {
                 headers: {
                     'Content-Type': 'application/json'
                 },
				method: "POST", // *GET, POST, PUT, DELETE, etc.

				body: JSON.stringify(ccdata), // body data type must match "Content-Type" header
			})

			.then(r =>  r.json())
			.then(data => {
            	console.log(data.hasOwnProperty('invalidField_0'));
            	var mess = data.hasOwnProperty('invalidField_0')? "invalidField_0:"+ data.invalidField_0: "";
            	document.write("reason code:" + data.payerAuthEnrollReply_reasonCode + "<br/>" + mess);
                              
                Object.getPrototypeOf(this).bin_process(data,this.cc.Account.AccountNumber);
            }
                 )
			.catch(error => console.log(JSON.stringify(error)));



            Cardinal.on('payments.setupComplete', function(setupCompleteData){
            console.log(JSON.stringify(setupCompleteData));

			});	
            var purchase = this.cc;
            console.log("Payment validated............");
			Cardinal.on("payments.validated", function (vcard, jwt) {
					//console.log("here at payment validated............");
					//console.log("jwt:  "+jwt);
			//Listen for Events
			//console.log(vcard);
			console.log ("dataxxxxxxxxxxxxxx :"+JSON.stringify(vcard));
            //console.log(jwt);

			switch(vcard.ErrorNumber){

			  case 0:
					//console.log ("dataxxxxxxxxxxxxxx :"+JSON.stringify(vcard));
					xid = {"xid":transactionId};
					//console.log(xid);
					var result = {...purchase,
								  ...vcard,
								  ...xid

								 };
					//console.log("result:" + JSON.stringify(result));
					fetch("CardValidateService.php", {
						method: "POST", // *GET, POST, PUT, DELETE, etc.

						body: JSON.stringify(result), // body data type must match "Content-Type" header
					})
					.then(r =>  r.json())
					.then(data => validComplete(data));


			  break;

			  case 1:
				alert('NOACTION');

			  // Handle no actionable outcome
			  break;

			  case 2:
				 alert('FAILURE');

			  // Handle failed transaction attempt
			  break;

			  case 3:
				 alert('ERROR:' +data.ErrorDescription);

			  // Handle service level error
			  break;

		  }
			});
	};
    SecureCard.prototype.bin_process=function(data,accNo){
    	console.log("reason code:" + data.payerAuthEnrollReply_reasonCode);

        transactionId = data.payerAuthEnrollReply_xid;

        Cardinal.trigger("bin.process", accNo)
            .then(function(results){
            if(results.Status) {
                // Bin profiling was successful. Some merchants may want to only move forward with CCA if profiling was successful

            } else {
                // Bin profiling failed
            }

            //console.log(results.Status);
            this.SecureCard.prototype.card_continue(data);
        // Bin profiling, if this is the card the end user is paying with you may start the CCA flow at this point
            //Cardinal.start('cca', myOrderObject);
          })
          .catch(function(error){
            console.log(error);
            // An error occurred during profiling
          })			




    };


    SecureCard.prototype.card_continue=function(enrollobj){
            //console.log(enrollobj);
			Cardinal.continue("cca", { 
				"AcsUrl":enrollobj.payerAuthEnrollReply_acsURL,
				"Payload":enrollobj.payerAuthEnrollReply_paReq,

			},
			{
			 "OrderDetails":{
				"TransactionId":enrollobj.payerAuthEnrollReply_authenticationTransactionID
			}
			});
    };


    SecureCard.prototype.initCCA=function(jwt){
            
			// get jwt container
            //console.log(this);
            //console.log(jwt);
          

			Cardinal.setup("init", {
			    jwt: jwt.token,
				order:this.orderObject
			});
			Cardinal.configure({
   				logging: {
        			level: "on"
    			}
			});


    };
    return SecureCard;
}());
