# Proposed REST Integration
### The Steps entail 
 - Merchants enabled for REST will include a Javascript file that contains the function for Secure3D namely
 - initiating interaction with Cardinal
 - Token Generation
 -  Enrollement Check
 -  Validation, Authorization and Settlement

The javascript file will be triggered as follows in 
```html
    <script src="https://ipay-staging.ipayafrica.com/ipay_rest/js/restcc-1.0.min.js"></script>
```
The script will in turn launch Cardinal’s songbird
```
    https://songbirdstag.cardinalcommerce.com/cardinalcruise/v1/songbird.js;
```
Card data is passed to the javascript class SecureCard as follows

```js
		try {
            let sc = new SecureCard(jsonData);
            
            sc.enable();
		}catch(err) {
			console.log(err);
		}
```
### The Process
1. The `sc.enable` function initiates creation of the JWT by calling the **ClaimRequest.php**

2. The token is used to initiate Cardinal and proceed with an enrollment check by calling the **CardAuthEnrollService.php**.

3. Enrolled Cards initiate the **BIN Detection Process** and validation is commenced using the **CardValidateService.php**.

*In all this requests card data is passed from one function to the next without storage.*

Other aspects such as use of SID can be built on top of this basic funcitons.

Example : **REST01.html**