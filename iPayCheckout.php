﻿<!DOCTYPE HTML>
<html lang="en"><head>
 
   
    <meta charset="utf-8">         
    <meta http-equiv="X-UA-Compatible" content="IE=edge">       
<meta name="robots" content="noindex">    
<meta name="viewport" content="width=device-width, initial-scale=1">         
<!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags --> 
<title>eLipa Checkout</title>     <!-- Bootstrap -->     
    <link href="assets/css.css" rel="stylesheet" type="text/css">   
    <link href="assets/bootstrap.min.css" rel="stylesheet">   
    <link href="assets/loading-bar.min.css" rel="stylesheet">     
    <link href="assets/custom.css" rel="stylesheet"> 

<style type="text/css">
body {
  font-family: 'Ubuntu', sans-serif;
}
[ng\:cloak], [ng-cloak], .ng-cloak {
  display: none !important;
}
.glyphicon.spinning {
    animation: spin 1s infinite linear;
    -webkit-animation: spin2 1s infinite linear;
}

@keyframes spin {
    from { transform: scale(1) rotate(0deg); }
    to { transform: scale(1) rotate(360deg); }
}

@-webkit-keyframes spin2 {
    from { -webkit-transform: rotate(0deg); }
    to { -webkit-transform: rotate(360deg); }
}
    
</style>
     <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries --> 
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->   
  <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]--> 
        <!-- Global site tag (gtag.js) - Google Analytics --> 
<script src="assets/js.js" async=""></script>


<script>

    function setCurrency(){
    
        var selcountry= document.getElementById('country').value;
        var disp = "";
        switch(selcountry){
            case "US":
                disp = 'USD';
        		var total = document.getElementById('total');
                var dispamt= document.getElementById('dispamount');
                total.value="1.00";
        		dispamt.innerHTML=" " + total.value;
                                                     	
				break;

            case "UG":
                disp = 'UGX';
                break;
            case "TZ":
                disp = 'TZS';
                break;
            default:
                disp = 'KES';
        }
        var newcurr = document.getElementById('curr');
        newcurr.innerHTML=" "+disp;
    }

  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-126569689-1');
</script>
</head>
<body ng-controller="mainController">
<div class="visible-xs visible-sm" id="load"></div><noscript>&lt;div 
class="alert alert-danger" role="alert"&gt;JAVAscript IS OFF. Some functions may 
not work as required Please enable Javascript&lt;/div&gt;</noscript> 
<div class="row">
<div class="col-md-9 col-md-offset-1">
<div>
<div id="orderDetails" style="background: rgb(255, 255, 255); padding: 15px; border-radius: 6px;">

<div class="row">
<div class=" col-md-8 col-xs-12 col-sm-12">
<div>
<h1><img class="img-responsive" 
src="assets/eLipa.png"></h1></div></div>
<div class="col-md-4 col-xs-12 col-sm-12">
<div class="col-md-12 col-xs-6 col-sm-6">

<?php 
    $oid=substr(hexdec(uniqid()),0,12);
    $amt = 200
?>
    <h3 id="dispamt"><small><b>Total<span id="curr"> UGX</span> <span id="dispamount"><?php echo sprintf("%01.2f", $amt); ?></span>
<div id="order_amount_new"></div></b></small></h3></div>
<div class="col-md-12 col-xs-6 col-sm-6">
<h3><SMALL><b>Order ID: <?php echo $oid;?> </b></SMALL></h3></div></div></div></div>
<div class="row">
<div class="col-md-12 well"><!-- Nav tabs -->                           
<div class="row">
<div class="col-md-12">

<div id="amount" style="display: none;"></div>

<div class="tab-content">
<div class="tab-pane  active" id="creditcard" role="tabpanel"><!-------------------------------------------------------------- - --> 
                              
<form name="visaForm"  style="background: rgb(255, 255, 255); padding: 15px; border-radius: 6px;" 
action="ipaycc.php" method="POST"><!--<form name="visaForm" style="background:#fff; padding:15px; border-radius:6px;" action="https://payments.ipayafrica.com/ipycc_tokenize.php" method="POST" >--> 
                    
    <input name="order_id" type="hidden" readonly="" value="<?php echo $oid ?>">          
<input name="inv" type="hidden" readonly="" value="<?php echo $oid ?>">          
<input name="total" id="total" type="hidden" readonly="" value="<?php echo $amt ?>">
<input name="origin" id="origin" type="hidden" readonly="" value="">

            
<div class="row">
<div class="col-xs-12">
<div class="row">
<div class="col-xs-12">
<div class="form-group"><label for="card_no_input">Card Number</label>           
            
<input name="ccnum" title="click to enter your card number here" class="form-control" id="visa_ccnum" required="" type="text" maxlength="16" placeholder="Card Number" pattern="^(?:4[0-9]{15}(?:[0-9]{3})?|5[1-5][0-9]{14})$" value="" data-toggle="tooltip" nosave="" data-placement="top"><!-- <input type="text" class="form-control" placeholder="Card Number" required maxlength="16" data-toggle="tooltip" data-placement="top" title="click to enter your card number here" nosave name="ccnum" ng-focus="spaceData($event)" ng-blur="removespaceData($event)" ng-model="ccnum"> --> 
    </div></div></div></div></div>
<div class="row">
<div class="col-md-6">
<div class="form-group"><label for="card_no_input">Expiry</label>                
     
<div class="row">
    <div class="col-xs-6"><input name="ccmonth" class="form-control" id="card_no_mmonth" required="" type="text" maxlength="2" placeholder="MM" value=""></div>
    <div class="col-xs-6"><input name="ccyear" class="form-control" id="card_no_year" required="" type="text" maxlength="2" placeholder="YY" value=""></div></div></div></div>
<div class="col-md-5 col-md-offset-1">
<div class="form-group"><label for="cvv_no">Security Code</label>                
    <input name="cvv" title="click to enter your CVV number here. It is normally a 3 or 4 digit number at the back of your card" class="form-control" id="cvv_no" required="" type="password" size="6" maxlength="4" placeholder="CVV" value="" data-toggle="tooltip" data-placement="top"></div></div></div>
    
<div class="row">
<div class="col-md-12">
<div class="form-group"><label for="card_no_input">Card Holder Name</label>      
           
<div class="row">
<div  class="col-md-5 col-sm-12"><input name="cust_fname" class="form-control" id="fname" type="text" placeholder="Firstname" value=""></div>&nbsp; 
                     
<div 
     class="col-md-6 col-sm-12"><input name="cust_lname" class="form-control" id="lname" type="text" placeholder="Lastname" value=""></div></div></div></div></div>
<div class="row">
<div class="col-md-12">
    <div class="form-group"><label for="contact_input"></label>
<div class="row">
<div class="col-md-5 col-sm-12"><label for="email_input">Email Address</label>           
        
    <input name="email" class="form-control" id="email_input" required="" type="email" placeholder="Email Address" value="tests@gmail.com">
</div>
<div class="col-md-6 col-sm-12"><label for="phone_input">Phone</label>           
    <input name="phone" class="form-control" id="phone_input" required="" type="text" placeholder="2547XXXXXXXX" value="">
    </div>
    </div>
    </div></div>
</div>
    <input name="cust_City" class="form-control" id="phone_input" required="" type="hidden" readonly="" value="">
<div class="row">
<div class="col-md-12">
    <div class="form-group"><label for="location_input"></label>
<div class="row">

<div class="col-md-5 col-sm-12"><label for="street_input">Street</label>   

    <input name="street" class="form-control" id="street_input" required="" type="text" placeholder="" value="">
</div>    
<div class="col-md-6 col-md-12">

    <label for="country_input">Currency</label>                   
    <select name="cust_Country" class="form-control" id="country" required="" onchange="setCurrency();">
        <option selected="selected" value="">Please Pick Country</option>
            <option value="KE">Kenya(KES)</option>                     
            <option value="UG">Uganda(UGX)</option>   
            <option value="TZ">Tanzania(TZS)</option>
            <option value="US">United States(USD)</option>                     

    </select></div>

        </div></div></div></div>
&nbsp; 
<div class="row">
<button class="btn btn-default btn-block btn-primary" id="visa_btn" type="submit">
    <b>Confirm Payment Done</b></button></div>
    </form></div></div></div></div></div></div></div></div></div>
<style type="text/css">
    #load{
    width:100%;
    height:100%;
    position:fixed;
    z-index:9999;
    /*background: #ccc no-repeat center center;*/
    background:url("https://www.creditmutuel.fr/cmne/idfr/banques/webservices/nswr/images/loading.gif") no-repeat center center rgba(0,0,0,0)
}
</style>
     <!-- jQuery (necessary for Bootstrap's Javascript plugins) -->    <!--<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>--> 
    <!-- Include all compiled plugins (below), or include individual files as needed --> 
    
<script src="assets/jquery-1.11.3.min.js"></script>
     
<script src="assets/bootstrap.min.js"></script>
     
     
<script>

     
      $(window).load(function() {
      if ( $('#visa_btn').length > 0 ) {
      $('#visa_btn').prop('disabled', false);
      }
      });

      $('form[name="visaForm"]').submit(function(){
      $(this).find('#visa_btn').prop('disabled', true);
      });
     </script>
      
<script type="text/javascript">
    var script = document.createElement('script');
    script.type = 'text/javascript';
    script.src = 'https://geolocation-db.com/json/geoip.php?jsonp=origination';
    var h = document.getElementsByTagName('script')[0];
    h.parentNode.insertBefore(script, h);

    function origination(data){
        var origin = document.getElementById('origin');
        origin.value= data.country_code;
     }
     $('#visa_ccnum').bind('change keydown keyup',function (event){
    var regstr = /[0-9]$/;            
    
    if (isNaN(event.target.value) || $(this).val() == "") {
      //alert('not a number');
      event.target.parentElement.classList.add('has-error');
     $(this).focus();
      console.log('sio number');
      console.log(event.target.value);
      $("input#visa_ccnum").focus();
      // event.preventDefault();
    }else{
      event.target.parentElement.classList.remove('has-error')
    }

      // if(event.which != 8 && isNaN(String.fromCharCode(event.which))){
      //     event.preventDefault();
      // }//prevents any other key press other than back space and numbers
          if(event.which != 8){//if backspace isnt pressed
              if($(this).val().length  == $(this).attr('maxlength') ) {
              $("input#card_no_mmonth").focus();
              }
          }
    
});

$('#card_no_mmonth').bind('change keydown keyup',function (event){
      if(event.which != 8){//if backspace isnt pressed
              if($(this).val().length  == $(this).attr('maxlength') ) {
              $("input#card_no_year").focus();
              //alert('sema ime fika');
              }
          }
});

$('#card_no_year').bind('change keydown keyup',function (event){
      if(event.which != 8){//if backspace isnt pressed
              if($(this).val().length  == $(this).attr('maxlength') ) {
              $("input#cvv_no").focus();
              //alert('sema ime fika');
              }
          }
});
     </script>
     
<script type="text/javascript">
      $(function () {
        //alert('test');
  $('[data-toggle="tooltip"]').tooltip()
})
            $(function () {
        //alert('test');
  $('#show_manual_suggestion').tooltip()
})
    $('.btn-danger').click(function(e){
        showmodal();        
 
    });
    function disabler()
    {
        document.getElementById("btn").disabled = true;
        document.getElementById("btn2").disabled = true;
    }
    function showmodal()
      {
        document.getElementById('load').style.visibility = 'visible';
        //document.getElementById('btn').disabled = true;
      }
      document.onreadystatechange = function () {
  var state = document.readyState
  if (state == 'interactive') {
         //document.getElementById('load').style.visibility = 'visible';
  } else if (state == 'complete') {
      setTimeout(function(){
         document.getElementById('load').style.visibility="hidden";
        // document.getElementById("btn").disabled = false;
         //document.getElementById("btn2").disabled = false;
      },1000);
  }
}
    </script>
       

    </body>
            </html>
